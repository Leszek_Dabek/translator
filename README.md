# Translator
Zbiór projektów modelujących jedną z metod translacji podstawieniowej, tu z kluczem 'gaderypoluki'. Każda implementacja translatora dostarcza przynajmniej jednej metody translacji wraz z metodami dodatkowymi. Implementacje są naiwne, z reguły nie potrafią sobie poradzić z rozróżnianiem wielkości liter.

Projekty związane z translatorem służą do nauki testów jednostkowych oraz wybranych technologii towarzyszących (testy parametryczne, różne biblioteki, asercje), które pomagają tworzyć lepsze testy oraz poszerzają spektrum ogólnie rozumianych umiejętności programistycznych. Dodatkowo projekt może stanowić podstawę do pracy z TDD.
## Technologie i projekty
### atranslator
Angular + TypeScript
### ptranslator
Python + unittest/pytest
### jtranslator
Java8 + Junit 5
## Dodatkowe informacje
https://pl.wikipedia.org/wiki/Gaderypoluki
